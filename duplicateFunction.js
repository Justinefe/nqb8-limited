const returnDuplicate = (duplicateElement) => {
  let ordered = [];
  duplicateElement.reduce((accumulator, currentValue) => {
    if (accumulator.indexOf(currentValue) === -1) {
      ordered.push(currentValue);
    }
    if (accumulator.indexOf(currentValue) !== -1) {
      accumulator.push(currentValue);
    }
    return accumulator;
  }, []);
};
console.log(returnDuplicate(duplicateElement));
